Create Database mysk;

CREATE TABLE Song (
    id int GENERATED ALWAYS AS IDENTITY,
    name varchar(255),
    artist varchar(255),
    originalFileName varchar(255) UNIQUE,
    fullPath varchar(255),
    saved_at varchar(255),
    created_at varchar(255)
);


alter table song
	add rating int default 50 not null;

CREATE TABLE Tag (
    id int GENERATED ALWAYS AS IDENTITY,
    name varchar(255) UNIQUE
);

CREATE TABLE SongTag ( 
    song_id int,
    tag_id int
);

ALTER TABLE SongTag
ADD CONSTRAINT UQ_SongId_TagId UNIQUE(song_id, tag_id)

create index song_originalfilename_index
	on song (originalfilename);

CREATE TABLE stats (
    song_id int UNIQUE NOT NULL,
    played_seconds int default 0 not null,
    skipped_count int
);
