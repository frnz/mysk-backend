package web

type SongDTO struct {
	Id               int      `json:"id"`
	Name             string   `json:"name"`
	Artist           string   `json:"artist"`
	OriginalFileName string   `json:"originalFileName"`
	FullPath         string   `json:"fullPath"`
	SavedAt          string   `json:"savedAt"`
	CreatedAt        string   `json:"creationDate"`
	Tags             []string `json:"tags"`
}
