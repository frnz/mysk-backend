package web

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"mysk/persistence"
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/gorilla/mux"
)

// HandleRequests starts the rest endpoints
func HandleRequests() {
	router := mux.NewRouter().StrictSlash(true)
	router.HandleFunc("/songs", addSong).Methods("POST")
	router.HandleFunc("/songs/{id}/rating/{rating}", updateRating).Methods("PUT")
	router.HandleFunc("/songs/{song_id}/played/{seconds_played}", updateStats).Methods("PUT")
	router.HandleFunc("/songs", getSongs).Methods("GET")
	router.HandleFunc("/tags", addSong)

	log.Println("Waiting for http-requests ...\n-------------------")
	log.Fatal(http.ListenAndServe(":8888", router))
}

func addSong(w http.ResponseWriter, r *http.Request) {
	log.Println("[POST] Create song.")
	reqBody, _ := ioutil.ReadAll(r.Body)
	var song SongDTO
	err := json.Unmarshal(reqBody, &song)
	logError(err, "Error while parsing song json.")

	now := time.Now().UTC().Format(time.RFC3339)
	songRecord := persistence.SongRecord{
		Name:             song.Name,
		Artist:           song.Artist,
		OriginalFileName: song.OriginalFileName,
		FullPath:         song.FullPath,
		SavedAt:          now,
		CreatedAt:        song.CreatedAt,
	}
	record := songRecord

	songId := persistence.SaveSong(record)
	tagIds := persistence.SaveTag(toRecord(song.Tags))
	persistence.AddTagsToSong(songId, tagIds)

	_, _ = fmt.Fprintf(w, "Saved song.")
}

func updateRating(w http.ResponseWriter, r *http.Request) {
	log.Println("[PUT] Update rating.")
	vars := mux.Vars(r)

	songId, err := strconv.Atoi(vars["id"])
	logError(err, "id is not an integer")
	rating, err := strconv.Atoi(vars["rating"])
	logError(err, "rating is not an integer")
	persistence.UpdateRating(songId, rating)

	_, _ = fmt.Fprintf(w, "Updated rating.")
}

func updateStats(w http.ResponseWriter, r *http.Request) {
	log.Println("[PUT] Update stats.")
	vars := mux.Vars(r)

	songId, err := strconv.Atoi(vars["song_id"])
	logError(err, "song_id is not an integer")
	rating, err := strconv.Atoi(vars["seconds_played"])
	logError(err, "seconds_played is not an integer")
	persistence.UpdateTimePlayed(songId, rating)

	_, _ = fmt.Fprintf(w, "Updated rating.")
}

func getSongs(w http.ResponseWriter, r *http.Request) {
	log.Println("[GET] Get all songs.")

	originalfilename := r.URL.Query().Get("originalfilename")
	existsByFilename := r.URL.Query().Get("exists")
	tagQuery := r.URL.Query().Get("tags")
	nameQuery := r.URL.Query().Get("namequery")
	artistQuery := r.URL.Query().Get("artist")
	tags := strings.Split(tagQuery, ",")

	var songs []persistence.SongEntity
	if originalfilename != "" {
		songs = persistence.GetSongsByOriginalFileName(originalfilename)
	} else if existsByFilename != "" {
		exists := persistence.ExistsSongsByOriginalFileName(existsByFilename)
		_, _ = fmt.Fprintf(w, strconv.FormatBool(exists))
		return
	} else if tagQuery != "" && len(tags) != 0 {
		songs = persistence.GetSongsByTags(tags)
	} else if nameQuery != "" {
		songs = persistence.GetSongsByNameQuery(nameQuery)
	} else if artistQuery != "" {
		songs = persistence.GetSongsByArtist(artistQuery)
	} else {
		songs = persistence.GetAllSongs()
	}
	result, _ := json.Marshal(&songs)
	_, _ = fmt.Fprintf(w, string(result))
}

func toRecord(tags []string) []persistence.TagRecord {
	tagRecords := make([]persistence.TagRecord, len(tags))
	for i := 0; i < len(tags); i++ {
		tagRecords[i] = persistence.TagRecord{
			Name: tags[i],
		}
	}
	return tagRecords
}

func logError(err error, text string) {
	if err != nil {
		log.Println(text, err.Error())
	}
}
