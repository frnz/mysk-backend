package persistence

type SongEntity struct {
	Id               int      `json:"id"`
	Name             string   `json:"name"`
	Artist           string   `json:"artist"`
	OriginalFileName string   `json:"originalFileName"`
	FullPath         string   `json:"fullPath"`
	SavedAt          string   `json:"savedAt"`
	CreatedAt        string   `json:"createdAt"`
	Rating           int      `json:"rating"`
	Tags             []string `json:"tags"`
}
