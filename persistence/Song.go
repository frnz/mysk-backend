package persistence

// SongRecord is a database type for songs
type SongRecord struct {
	Id               int
	Name             string
	Artist           string
	OriginalFileName string
	FullPath         string
	SavedAt          string
	CreatedAt        string
}

// TagRecord is a database type for tags
type TagRecord struct {
	Id   int
	Name string
}

// SongTagRecord is a database type for song-tags
type SongTagRecord struct {
	SongId int
	TagId  int
}
