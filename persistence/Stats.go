package persistence

// Stats is a database type for statistics
type Stats struct {
	Song_id        int
	played_seconds int
	skipped_count  int
}
