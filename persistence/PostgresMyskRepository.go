package persistence

import (
	"context"
	"github.com/jackc/pgx/v4"
	"log"
	"strconv"
)

var databaseurl = "postgresql://postgres:postgres@127.0.0.1:5432/mysk"

// ############## WRITE ##################

func SaveSong(song SongRecord) int {
	//databaseurl :=  os.Getenv("DATABASE_URL")

	log.Printf("Saving song in postgres [%s]", song.OriginalFileName)

	conn := connect()
	defer conn.Close(context.Background())

	query := "INSERT INTO Song (name,artist,originalFileName,fullPath,saved_at,created_at) VALUES( $1, $2, $3, $4, $5, $6) RETURNING id"

	var id int64
	err := conn.QueryRow(context.Background(), query, song.Name, song.Artist, song.OriginalFileName, song.FullPath, song.SavedAt, song.CreatedAt).Scan(&id)
	logAndPanic(err, "QueryRow failed")

	log.Printf("Saved song [%s] with id [%d]", song.Name, id)
	return int(id)
}

func SaveTag(tags []TagRecord) []int {
	conn := connect()
	defer conn.Close(context.Background())

	//query := "INSERT INTO tag (name) VALUES( $1) RETURNING id"
	query :=
		`with s as (
			select id, name
			from tag
			where name = $1
		), i as (
			insert into tag ("name")
			select $1
			where not exists (select 1 from s)
			returning id, name
		)
		select id
		from i
		union all
		select id
		from s`

	resultIds := make([]int, len(tags))
	for i, tag := range tags {
		log.Printf("Saving tag in postgres [%s]", tag.Name)
		var id int64
		err := conn.QueryRow(context.Background(), query, tag.Name).Scan(&id)
		logAndPanic(err, "QueryRow failed")
		log.Printf("Saved tag [%s] with id [%d]", tag.Name, id)
		resultIds[i] = int(id)
	}

	return resultIds
}

func AddTagsToSong(songId int, tagIds []int) {

	conn := connect()
	defer conn.Close(context.Background())

	query := "INSERT INTO songtag (song_id,tag_id) VALUES( $1, $2) RETURNING 1"
	for _, tag := range tagIds {
		log.Printf("Saving songTag in postgres [song=%d | tag=%d]", songId, tag)
		var id int64
		err := conn.QueryRow(context.Background(), query, songId, tag).Scan(&id)
		logAndPanic(err, "QueryRow failed")
		log.Printf("Saved songTag [song=%d | tag=%d]", songId, tag)
	}
}

func UpdateRating(songId int, rating int) {
	conn := connect()
	defer conn.Close(context.Background())

	query :=
		`UPDATE song 
			SET rating = $1
		WHERE id = $2
		RETURNING 1
`
	var id int64
	err := conn.QueryRow(context.Background(), query, rating, songId).Scan(&id)
	logAndPanic(err, "QueryRow failed")
	log.Printf("Updated rating of song with id [%d] to new rating [%d]", songId, rating)

}

func UpdateTimePlayed(songId int, secondsPlayed int) {
	conn := connect()
	defer conn.Close(context.Background())

	rows, err := conn.Query(context.Background(), "SELECT song_id,played_seconds FROM  stats where song_id = $1", songId)
	logAndPanic(err, "Getting previous songs stats from sql failed.")
	result := make([]Stats, 0)
	for rows.Next() {
		var songIdDb, secondsPlayedDb int
		if errSql := rows.Scan(&songIdDb, &secondsPlayedDb); errSql != nil {
			log.Fatal(errSql)
		}
		resultStats := Stats{
			Song_id:        songIdDb,
			played_seconds: secondsPlayedDb,
		}
		result = append(result, resultStats)
	}

	var query string
	updatedSecondsPlayed := secondsPlayed
	if len(result) == 0 {
		log.Printf("Song %d is new in stats. Inserting new entry!", songId)
		query =
			`INSERT INTO stats (song_id, played_seconds)
			 VALUES($1,$2)
`
	} else {
		updatedSecondsPlayed = secondsPlayed + result[0].played_seconds
		log.Printf("Song %d is already known in stats. Updating old:%d + new:%d = total:%d", songId, result[0].played_seconds, secondsPlayed, updatedSecondsPlayed)
		query =
			`UPDATE stats 
			 SET played_seconds = $2
		     WHERE song_id = $1
`
	}

	_ = conn.QueryRow(context.Background(), query, songId, updatedSecondsPlayed).Scan()
	logAndPanic(err, "QueryRow failed")
	log.Printf("Updated seconds played of song with id [%d] increased by [%ds] to [%ds]", songId, secondsPlayed, updatedSecondsPlayed)

}

// ############## READ ##################

func GetAllSongs() []SongEntity {

	conn := connect()
	defer conn.Close(context.Background())

	query := `select song.id
     , song.name
     , artist
     , originalfilename
     , fullpath
     , saved_at
     , created_at
     , rating
     , array_agg(t.name) as tags
from song
         LEFT JOIN songtag s on song.id = s.song_id
         LEFT JOIN tag t on s.tag_id = t.id
group by song.id, song.name
       , artist
       , originalfilename
       , fullpath
       , saved_at
       , rating
       , created_at`
	log.Printf("Getting all songs in postgres.")
	rows, errSql := conn.Query(context.Background(), query)
	logAndPanic(errSql, "Getting all songs from sql failed")
	defer rows.Close()

	result := make([]SongEntity, 0)
	for rows.Next() {
		var name, artist, originalfilename, fullpath, savedat, createdat string
		var id, rating int
		var tags []string
		if errSql = rows.Scan(&id, &name, &artist, &originalfilename, &fullpath, &savedat, &createdat, &rating, &tags); errSql != nil {
			log.Fatal(errSql)
		}
		resultSong := SongEntity{
			Id:               id,
			Name:             name,
			Artist:           artist,
			OriginalFileName: originalfilename,
			FullPath:         fullpath,
			SavedAt:          savedat,
			CreatedAt:        createdat,
			Rating:           rating,
			Tags:             tags,
		}
		result = append(result, resultSong)
	}
	log.Printf("Got all %d songs in postgres.", len(result))
	return result
}

func GetSongsByTags(tags []string) []SongEntity {
	if len(tags) == 0 {
		return make([]SongEntity, 0)
	}

	conn := connect()
	defer conn.Close(context.Background())

	query := `select song.id
     , song.name
     , artist
     , originalfilename
     , fullpath
     , saved_at
     , created_at
     , rating
     , array_agg(t.name) as tags
from song
         LEFT JOIN songtag s on song.id = s.song_id
         LEFT JOIN tag t on s.tag_id = t.id
group by song.id, song.name
       , artist
       , originalfilename
       , fullpath
       , saved_at
       , created_at
       , rating
having $1 = ANY (array_agg(t.name))`

	for i := 2; i < len(tags)+1; i++ {
		iter := strconv.Itoa(i)
		query = query + "AND $" + iter + " = ANY (array_agg(t.name))"
	}

	tagInterfaces := make([]interface{}, len(tags))
	for i, v := range tags {
		tagInterfaces[i] = v
	}

	log.Printf("Getting all songs in postgres.")
	rows, errSql := conn.Query(context.Background(), query, tagInterfaces...)
	logAndPanic(errSql, "Getting all songs from sql failed.")
	defer rows.Close()

	result := make([]SongEntity, 0)
	for rows.Next() {
		var name, artist, originalfilename, fullpath, savedat, createdat string
		var id, rating int
		var tags []string
		if errSql = rows.Scan(&id, &name, &artist, &originalfilename, &fullpath, &savedat, &createdat, &rating, &tags); errSql != nil {
			log.Fatal(errSql)
		}
		resultSong := SongEntity{
			Id:               id,
			Name:             name,
			Artist:           artist,
			OriginalFileName: originalfilename,
			FullPath:         fullpath,
			SavedAt:          savedat,
			CreatedAt:        createdat,
			Rating:           rating,
			Tags:             tags,
		}
		result = append(result, resultSong)
	}
	log.Printf("Got all %d songs in postgres.", len(result))
	return result
}

func GetSongsByNameQuery(nameQuery string) []SongEntity {

	conn := connect()
	defer conn.Close(context.Background())

	query := `select song.id
     , song.name
     , artist
     , originalfilename
     , fullpath
     , saved_at
     , created_at
     , rating
     , array_agg(t.name) as tags
from song
         LEFT JOIN songtag s on song.id = s.song_id
         LEFT JOIN tag t on s.tag_id = t.id
where song.name ILIKE $1
group by song.id, song.name
       , artist
       , originalfilename
       , fullpath
       , saved_at
       , created_at
       , rating`
	log.Printf("Getting all songs in postgres.")
	rows, errSql := conn.Query(context.Background(), query, "%"+nameQuery+"%")
	logAndPanic(errSql, "Getting all songs from sql failed")
	defer rows.Close()

	result := make([]SongEntity, 0)
	for rows.Next() {
		var name, artist, originalfilename, fullpath, savedat, createdat string
		var id, rating int
		var tags []string
		if errSql = rows.Scan(&id, &name, &artist, &originalfilename, &fullpath, &savedat, &createdat, &rating, &tags); errSql != nil {
			log.Fatal(errSql)
		}
		resultSong := SongEntity{
			Id:               id,
			Name:             name,
			Artist:           artist,
			OriginalFileName: originalfilename,
			FullPath:         fullpath,
			SavedAt:          savedat,
			CreatedAt:        createdat,
			Rating:           rating,
			Tags:             tags,
		}
		result = append(result, resultSong)
	}
	log.Printf("Got all %d songs in postgres.", len(result))
	return result
}

func GetSongsByArtist(artist string) []SongEntity {

	conn := connect()
	defer conn.Close(context.Background())

	query := `select song.id
     , song.name
     , artist
     , originalfilename
     , fullpath
     , saved_at
     , created_at
     , rating
     , array_agg(t.name) as tags
from song
         LEFT JOIN songtag s on song.id = s.song_id
         LEFT JOIN tag t on s.tag_id = t.id
where song.artist = $1
group by song.id, song.name
       , artist
       , originalfilename
       , fullpath
       , saved_at
       , created_at
       , rating`
	log.Printf("Getting all songs in postgres.")
	rows, errSql := conn.Query(context.Background(), query, artist)
	logAndPanic(errSql, "Getting all songs from sql failed")
	defer rows.Close()

	result := make([]SongEntity, 0)
	for rows.Next() {
		var name, artist, originalfilename, fullpath, savedat, createdat string
		var id, rating int
		var tags []string
		if errSql = rows.Scan(&id, &name, &artist, &originalfilename, &fullpath, &savedat, &createdat, &rating, &tags); errSql != nil {
			log.Fatal(errSql)
		}
		resultSong := SongEntity{
			Id:               id,
			Name:             name,
			Artist:           artist,
			OriginalFileName: originalfilename,
			FullPath:         fullpath,
			SavedAt:          savedat,
			CreatedAt:        createdat,
			Rating:           rating,
			Tags:             tags,
		}
		result = append(result, resultSong)
	}
	log.Printf("Got all %d songs in postgres.", len(result))
	return result
}

func GetSongsByOriginalFileName(originalfilename string) []SongEntity {

	conn := connect()
	defer conn.Close(context.Background())

	query := `select song.id
     , song.name
     , artist
     , originalfilename
     , fullpath
     , saved_at
     , created_at
     , rating
     , array_agg(t.name) as tags
from song
         LEFT JOIN songtag s on song.id = s.song_id
         LEFT JOIN tag t on s.tag_id = t.id
where song.originalfilename = $1
group by song.id, song.name
       , artist
       , originalfilename
       , fullpath
       , saved_at
       , created_at
       , rating`
	log.Printf("Getting all songs in postgres.")
	rows, errSql := conn.Query(context.Background(), query, originalfilename)
	logAndPanic(errSql, "Getting all songs from sql failed")
	defer rows.Close()

	result := make([]SongEntity, 0)
	for rows.Next() {
		var name, artist, originalfilename, fullpath, savedat, createdat string
		var id, rating int
		var tags []string
		if errSql = rows.Scan(&id, &name, &artist, &originalfilename, &fullpath, &savedat, &createdat, &rating, &tags); errSql != nil {
			log.Fatal(errSql)
		}
		resultSong := SongEntity{
			Id:               id,
			Name:             name,
			Artist:           artist,
			OriginalFileName: originalfilename,
			FullPath:         fullpath,
			SavedAt:          savedat,
			CreatedAt:        createdat,
			Rating:           rating,
			Tags:             tags,
		}
		result = append(result, resultSong)
	}
	log.Printf("Got all %d songs in postgres.", len(result))
	return result
}

func ExistsSongsByOriginalFileName(originalfilename string) bool {

	conn := connect()
	defer conn.Close(context.Background())

	query := `select count(*)
              from song
              where originalfilename = $1
`
	log.Printf("Counting songs by filename in postgres.")
	rows, errSql := conn.Query(context.Background(), query, originalfilename)
	logAndPanic(errSql, "Counting songs by filename from sql failed")
	defer rows.Close()

	result := make([]int, 0)
	for rows.Next() {
		var count int
		if errSql = rows.Scan(&count); errSql != nil {
			log.Fatal(errSql)
		}
		result = append(result, count)
	}
	log.Printf("Counted %d songs in postgres with for filename [%s].", len(result), originalfilename)
	return len(result) > 0 && result[0] > 0
}

// ############## UTIL ##################

func connect() *pgx.Conn {
	conn, err := pgx.Connect(context.Background(), databaseurl)
	logAndPanic(err, "Cold not connect to postgres database.")
	return conn
}

func logAndPanic(err error, text string) {
	if err != nil {
		log.Panicln(text, err)
	}
}
